\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Purpose and Scope}{3}
\contentsline {section}{\numberline {2}System Architecture Description}{4}
\contentsline {section}{\numberline {3}Code Structure}{7}
\contentsline {subsection}{\numberline {3.1}File/Folder Strcture}{7}
\contentsline {subsection}{\numberline {3.2}Style Guide}{8}
\contentsline {subsubsection}{\numberline {3.2.1}Logo}{8}
\contentsline {subsubsection}{\numberline {3.2.2}Fonts}{8}
\contentsline {subsubsection}{\numberline {3.2.3}Website Theme}{9}
\contentsline {subsubsection}{\numberline {3.2.4}Design Language}{9}
\contentsline {subsubsection}{\numberline {3.2.5}Pattern Library}{9}
\contentsline {subsubsection}{\numberline {3.2.6}Coding Style}{10}
\contentsline {section}{\numberline {4}Dependencies}{13}
\contentsline {section}{\numberline {5}Exclusions}{14}
\contentsline {subsection}{\numberline {5.1}Communication}{14}
\contentsline {subsection}{\numberline {5.2}Ranking}{14}
\contentsline {subsection}{\numberline {5.3}Album Support}{14}
\contentsline {subsection}{\numberline {5.4}Streaming}{14}
