\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Purpose}{3}
\contentsline {section}{\numberline {2}Project Plan}{4}
\contentsline {subsection}{\numberline {2.1}Roles for each Team Member}{4}
\contentsline {subsection}{\numberline {2.2}Milestones}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Milestone 1}{4}
\contentsline {subsubsection}{\numberline {2.2.2}Milestone 2}{4}
\contentsline {subsubsection}{\numberline {2.2.3}Milestone 3}{4}
\contentsline {subsection}{\numberline {2.3}Minimum Viable Product}{6}
\contentsline {section}{\numberline {3}Website Wireframe}{7}
\contentsline {section}{\numberline {4}Use Case Requirements}{10}
\contentsline {subsection}{\numberline {4.1}Use Case 1}{10}
\contentsline {subsection}{\numberline {4.2}Use Case 2}{10}
\contentsline {subsection}{\numberline {4.3}Use Case 3}{11}
\contentsline {subsection}{\numberline {4.4}Use Case 4}{11}
\contentsline {subsection}{\numberline {4.5}Use Case 5}{12}
\contentsline {subsection}{\numberline {4.6}Use Case 6}{13}
\contentsline {subsection}{\numberline {4.7}Use Case 7}{14}
\contentsline {subsection}{\numberline {4.8}Use Case 8}{15}
\contentsline {subsection}{\numberline {4.9}Use Case 9}{15}
\contentsline {subsection}{\numberline {4.10}Use Case 10}{16}
\contentsline {subsection}{\numberline {4.11}Diagrams}{16}
\contentsline {subsubsection}{\numberline {4.11.1}Use Case Diagram}{16}
\contentsline {subsubsection}{\numberline {4.11.2}Sequence Diagrams}{17}
\contentsline {section}{\numberline {5}References}{23}
