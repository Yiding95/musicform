#!/usr/bin/env python
# coding: utf-8

from setuptools import setup

setup(
    name="MusicForm",
    author="Gian Andres",
    author_email="gand6280@uni.sydney.edu.au",
    version="1.0",
    description="Music Form App",
    install_requires=[
        'Django == 1.10',
        'django-authtools'
    ]
)
