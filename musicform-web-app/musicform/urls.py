from django.conf.urls import url
from .views import band_view
from .views import profile_view
from .views import home_view

from .views.home_view import TemplateView
from .views.profile_view import LogoutDetail, ProfileDetail, AllSongsDetail

from django.utils.functional import curry
from django.views.defaults import *

urlpatterns = [
    # Registration URLs
    url(r'^register/$', profile_view.register, name='register'),
    # Login URLs
    url(r'^login/$', profile_view.login_form, name='login'),
    url(r'^logout/$', LogoutDetail.as_view(), name='logout'),
    # Band URLs
    url(r'^band/new/$', band_view.band_new, name='band_new'),
    url(r'^ajax/validate_bandname/$', band_view.validate_bandname, name='validate_bandname'),
    url(r'^band/(?P<band_id>[0-9]+)/edit/$', band_view.edit_band, name='edit_band'),
    url(r'^band/(?P<band_id>[0-9]+)/$', band_view.band_information, name='band_information'),
    url(r'^band/(?P<pk>[0-9]+)/edit_comment/$', band_view.edit_comment, name='edit_comment'),
    url(r'^band/(?P<pk>[0-9]+)/remove_comment/$', band_view.remove_comment, name='remove_comment'),
    url(r'^band/(?P<band_id>[0-9]+)/request/$', band_view.user_request, name='user_request'),
    url(r'^band/(?P<pk>[0-9]+)/approve_request/$', band_view.approve_request, name='approve_request'),
    url(r'^band/(?P<pk>[0-9]+)/decline_request/$', band_view.decline_request, name='decline_request'),
    # url(r'^band/request$', band_view.request, name='brequest'),
    url(r'^band/view_request/$', band_view.manage_request, name='manage_request'),
    # Navigation URLs
    url(r'^$', TemplateView.as_view(), name='home'),
    url(r'^status/$', band_view.status, name='status'),
    url(r'^request/$', band_view.request, name='request'),
    url(r'^search/$', home_view.search, name='search'),
    url(r'^about/$', TemplateView.as_view(template_name="musicform/about.html"), name='about'),
    url(r'^how_to/$', TemplateView.as_view(template_name="musicform/how_to.html"), name='how_to'),
    # Profile URLs
    url(r'^profile/$', ProfileDetail.as_view(), name='profile'),
    url(r'^profile/(?P<prof_id>[0-9]+)/$', profile_view.view_other_prof, name='view_profile'),
    url(r'^profile/edit_profile/$', profile_view.edit_profile, name='edit_profile'),
    url(r'^ajax/validate_username/$', profile_view.validate_username, name='validate_username'),
    url(r'^profile/genre/$', profile_view.user_genre, name='user_genre'),
    url(r'^profile/song/$', profile_view.user_song, name='user_song'),
    url(r'^profile/all_songs/$', AllSongsDetail.as_view(), name='all_songs'),
    url(r'^profile/song/([0-9]+)/$', profile_view.song_information, name='song_information'),
    url(r'^profile/instruments/$', profile_view.user_instruments, name='user_instruments'),
]

handler500 = curry(server_error, template_name='musicform/admin/500.html')
handler404 = curry(page_not_found, template_name='musicform/admin/404.html')
handler403 = curry(permission_denied, template_name='musicform/admin/403.html')
