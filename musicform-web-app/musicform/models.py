from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.urlresolvers import reverse
from datetime import datetime

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name="userprofile")

    join_date = models.DateTimeField(
        default=timezone.now
    )
    biography = models.TextField()
    birth_date = models.DateTimeField(
        default=timezone.now
    )
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    LOCATION_CHOICES = (
        ('Sydney', 'Sydney'),
        ('Canberra', 'Canberra'),
        ('Brisbane', 'Brisbane'),
        ('Melbourne', 'Melbourne'),
        ('Tasmania', 'Tasmania'),
        ('Darwin', 'Darwin'),
        ('Adelaide', 'Adelaide'),
        ('Perth', 'Perth'),
    )
    PROFILE_ICONS = (
        ('bells.png', 'Bell'),
        ('cloud.png', 'Cloud'),
        ('disc.png', 'Disc'),
        ('drum.png', 'Drums'),
        ('eq.png', 'Equaliser'),
        ('guitar.png', 'Guitar'),
        ('mic.png', 'Microphone'),
        ('piano.png', 'Piano'),
        ('sound.png', 'Sound'),
        ('soundboard.png', 'Soundboard'),
        ('tape.png', 'Tape'),
        ('tape2.png', 'Radio'),
        ('violin.png', 'Violin'),
        ('xylo.png', 'Xylophone'),
    )
    gender = models.CharField(max_length=50, choices=GENDER_CHOICES)
    location = models.CharField(max_length=50, choices=LOCATION_CHOICES)
    profile_icon = models.FileField(choices=PROFILE_ICONS, default="bells")
    mobile_number = models.CharField(max_length=12)

    def __unicode__(self):
        return self.user.username


class Requests(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id_from = models.ForeignKey('auth.User', related_name='sender')
    user_id_to = models.ForeignKey('auth.User', related_name='recipient')
    band = models.ForeignKey('musicform.Band', related_name='request', blank=False, null=True)
    text = models.TextField()
    approved = models.BooleanField(default=False)
    hide_function = models.BooleanField(default=False)
    date_created = models.DateTimeField(
        default=timezone.now
    )
    status = models.IntegerField(default=0)

    class Meta:
        unique_together = ('user_id_from', 'user_id_to', 'band',)

    def approve(self):
        self.approved = True
        self.hide_function = True
        self.save()

    def __str__(self):
        return self.text


class Song(models.Model):
    id = models.IntegerField(primary_key=True)
    uploader = models.ForeignKey('auth.User')
    filename = models.CharField(max_length=50)
    song_name = models.CharField(max_length=50)
    file = models.FileField()
    upload_date = models.DateTimeField(
        default=timezone.now
    )

    def get_absolute_url(self):
        return reverse('musicform:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.song_name


class Band(models.Model):
    id = models.IntegerField(primary_key=True)
    creator = models.ForeignKey('auth.User')
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=50, default="Sydney")
    date_created = models.DateTimeField(
        default=timezone.now
    )
    introduction = models.CharField(max_length=1000)
    members = models.IntegerField(default=0)
    logo = models.FileField(blank=True)

    def get_absolute_url(self):
        return reverse('musicform:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name

    def approved_members(self):
        return self.request.filter(approved=True)


class BandMembers(models.Model):
    id = models.IntegerField(primary_key=True)
    band = models.ForeignKey('musicform.Band', related_name='member', blank=False, null=True)
    author = models.ForeignKey('auth.User')

    def approve(self):
        self.save()


class BandComments(models.Model):
    id = models.IntegerField(primary_key=True)
    band = models.ForeignKey('musicform.Band', related_name='comments', blank=False, null=True)
    author = models.ForeignKey('auth.User')
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now
    )
    updated_date = models.DateTimeField(
        default=timezone.now
    )
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.save()

    def __str__(self):
        return self.text


class Instrument(models.Model):
    id = models.IntegerField(primary_key=True)
    owner = models.ForeignKey('auth.User')
    instrument = models.CharField(max_length=100)
    grade = models.IntegerField()

    def __str__(self):
        return self.instrument

    def get_absolute_url(self):
        return reverse('musicform:profile', kwargs={'pk': self.pk})

    class Meta:
        unique_together = ('owner', 'instrument')


class Genre(models.Model):
    GENRE_CHOICES = (
        ('Alternative', 'Alternative'),
        ('Classical', 'Classical'),
        ('Country', 'Country'),
        ('Electronic', 'Electronic'),
        ('Hip-Hop', 'Hip-Hop'),
        ('Rap', 'Rap'),
        ('Indie', 'Indie'),
        ('Jazz', 'Jazz'),
        ('Pop', 'Pop'),
        ('Soul', 'Soul'),
        ('Rock', 'Rock')
    )
    # id = models.IntegerField(primary_key=True)
    owner = models.ForeignKey('auth.User')
    genre = models.CharField(max_length=100, choices=GENRE_CHOICES)

    class Meta:
        unique_together = ('owner', 'genre',)

    def __str__(self):
        return self.genre