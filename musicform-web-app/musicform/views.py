# from django.shortcuts import render, get_object_or_404, render_to_response, redirect
#
# from django.http import HttpResponseRedirect, HttpResponseNotFound, Http404
# from django.http import JsonResponse
#
# from django.contrib import auth
# from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
# from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import User
#
# from django.db import models
# from django.db.models import Q
# from django.template import RequestContext
# from django.utils import timezone
# from django.contrib import messages
# from django.views import generic
# from django.views.generic import CreateView, View
#
# from django.urls import reverse
#
# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#
# from .models import Band, BandComments, Song, Instrument, Genre, Requests, UserProfile
# from .forms import UserForm, UserProfileForm, BandForm, CommentForm, SongForm, InstrumentForm, GenreForm, RequestForm
#
# IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
# SONG_FILE_TYPES = ['mp3', 'wav']
#
#
# # Administration Links
# def register(request):
#
#     context = RequestContext(request)
#
#     registered = False
#
#     user_form = UserForm(request.POST or None)
#     profile_form = UserProfileForm(request.POST or None)
#
#     if request.method == "POST":
#         if user_form.is_valid() and profile_form.is_valid():
#             user = user_form.save(commit=False)
#             user.set_password(user.password)
#             user.save()
#
#             profile = profile_form.save(commit=False)
#             profile.user = user
#             profile.save()
#             registered = True
#         else:
#             print (user_form.errors, profile_form.errors)
#     else:
#         user_form = UserForm()
#         profile_form = UserProfileForm()
#
#     value = {
#         'user_form': user_form,
#         'profile_form': profile_form,
#         'registered': registered,
#     }
#     return render(request, 'musicform/register.html', value, context)
#
#
# # Login URLs
# def login_form(request):
#     context = RequestContext(request)
#
#     if request.method == "POST":
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(username=username, password=password)
#         if user is not None:
#             if user.is_active:
#                 login(request, user)
#                 # bands = Bands.objects.filter(user=request.user)
#                 return HttpResponseRedirect('/profile/')
#             else:
#                 return HttpResponseRedirect("Your account has been disabled")
#         else:
#             context = {
#                 'error_message': 'Invalid Password/Username',
#             }
#             return render(request, 'musicform/login.html', context)
#     else:
#         return render(request, 'musicform/login.html', {}, context)
#
#
# @login_required
# def band_new(request):
#     if not request.user.is_authenticated():
#         return HttpResponseRedirect('/login/')
#     else:
#         form = BandForm(request.POST or None, request.FILES or None)
#         if form.is_valid():
#             band = form.save(commit=False)
#             band.published_date = timezone.now()
#             band.logo = request.FILES['logo']
#             band.creator = request.user
#             band.location = request.user.userprofile.location
#             file_type = band.logo.url.split('.')[-1]
#             file_type = file_type.lower()
#             if file_type not in IMAGE_FILE_TYPES:
#                 context = {
#                     'band': band,
#                     'error_message': 'Invalid Image Type',
#                 }
#                 return render(request, 'musicform/band/new_band.html', context)
#             band.save()
#             return HttpResponseRedirect('/status/')
#         context = {
#             "form": form
#         }
#         return render(request, 'musicform/band/new_band.html', context)
#
#
# def band_information(request, band_id):
#     user = request.user
#     band = get_object_or_404(Band, pk=band_id)
#     context = {
#         'user': user,
#         'band': band
#     }
#     return render(request, 'musicform/band/band_information.html', context)
#
# @login_required
# def edit_band(request, band_id):
#     band = get_object_or_404(Band, pk=band_id)
#     if request.method == "POST":
#         form = BandForm(request.POST, instance=band)
#         if form.is_valid():
#             band = form.save(commit=False)
#             band.creator = request.user
#             band.published_date = timezone.now()
#             band.save()
#             return HttpResponseRedirect(reverse('band_information', args=(band.id,)))
#     else:
#         form = BandForm(instance=band)
#     return render(request, 'musicform/band/edit_band.html', {'form': form})
#
#
# @login_required
# def edit_comment(request, pk):
#     comment = get_object_or_404(BandComments, pk=pk)
#     band_pk = comment.band.pk
#     if request.method == "POST":
#         form = CommentForm(request.POST, instance=comment)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.author = request.user
#             comment.updated_date = timezone.now()
#             comment.save()
#             return HttpResponseRedirect(reverse('band_information', args=(band_pk,)))
#     else:
#         form = CommentForm(instance=comment)
#     return render(request, 'musicform/band/edit_comment.html', {'form': form})
#
#
# @login_required
# def band_comment(request, band_id):
#     band = get_object_or_404(Band, pk=band_id)
#     if request.method == "POST":
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.author = request.user
#             comment.band = band
#             comment.save()
#             return HttpResponseRedirect(reverse('band_information', args=(band.id,)))
#     else:
#         form = CommentForm()
#     return render(request, 'musicform/band/comment.html', {'form': form})
#
#
# @login_required
# def remove_comment(request, pk):
#     comment = get_object_or_404(BandComments, pk=pk)
#     band_pk = comment.band.pk
#     comment.delete()
#     return HttpResponseRedirect(reverse('band_information', args=(band_pk,)))
#
# @login_required
# def comment_approve(request, pk):
#     comment = get_object_or_404(BandComments, pk=pk)
#     band_pk = comment.band.pk
#     comment.approve()
#     return HttpResponseRedirect(reverse('band_information', args=(band_pk,)))
#
#
# def send_request(request):
#     users = User.objects.all()
#     return render(request, 'musicform/band/send_request.html', {'users': users})
#
#
# @login_required
# def logout(request):
#     auth.logout(request)
#     return render(request, 'musicform/logout.html')
#
#
# # Navigation URLs
#
# def home(request):
#     return render(request, 'musicform/home.html')
#
#
# def status(request):
#     bands = Band.objects.all()
#     return render(request, 'musicform/status.html', {'bands': bands})
#
#
# @login_required
# def profile(request):
#     # user = get_object_or_404(User, username=username)
#     bands = Band.objects.filter(creator=request.user)
#     songs = Song.objects.filter(uploader=request.user)
#     instrument = Instrument.objects.filter(owner=request.user)
#     genre = Genre.objects.filter(owner=request.user)
#     context = {
#         'bands': bands,
#         'songs': songs,
#         'instruments': instrument,
#         'genres': genre,
#     }
#     return render(request, 'musicform/profile.html', context)
#
#
# @login_required
# def user_genre(request):
#     form = GenreForm(request.POST or None)
#     if form.is_valid():
#         genre = form.save(commit=False)
#         genre.owner = request.user
#         genre.save()
#         return HttpResponseRedirect('/profile/')
#     context = {
#         'genre_form': form
#     }
#     return render(request, 'musicform/profile/new_genre.html', context)
#
#
# @login_required
# def user_song(request):
#     form = SongForm(request.POST or None, request.FILES or None)
#     if form.is_valid():
#         song = form.save(commit=False)
#         song.uploader = request.user
#         file_type = song.file.url.split('.')[-1]
#         file_type = file_type.lower()
#         if file_type not in SONG_FILE_TYPES:
#             context = {
#                 'song': song,
#                 'error_message': 'Invalid File Type',
#             }
#             return render(request, 'musicform/profile/song.html', context)
#         song.save()
#         return HttpResponseRedirect('/profile/')
#     context = {
#         'form': form
#     }
#     return render(request, 'musicform/profile/new_song.html', context)
#
#
# @login_required
# def all_songs(request):
#     songs = Song.objects.filter(uploader=request.user)
#     return render(request, 'musicform/profile/all_songs.html', {'songs': songs})
#
#
# @login_required
# def user_instruments(request):
#     form = InstrumentForm(request.POST or None)
#     if form.is_valid():
#         instrument = form.save(commit=False)
#         instrument.owner = request.user
#         instrument.save()
#         return HttpResponseRedirect('/profile/')
#     context = {
#         'form': form
#     }
#     return render(request, 'musicform/profile/new_instrument.html', context)
#
# @login_required
# def song_information(request, song_id):
#     user = request.user
#     song = get_object_or_404(Song, pk=song_id)
#     context = {
#         'user': user,
#         'song': song
#     }
#     return render(request, 'musicform/profile/song.html', context)
#
#
# def search(request):
#     if request.method == "GET":
#         if request.GET.get('result'):
#             result = Band.objects.filter(name__contains=request.GET['result'])
#             if not result:
#                 context = {
#                     'bands': result,
#                 }
#             else:
#                 context = {
#                     'bands': result,
#                     'error_message': 'No results found'
#                 }
#             return render(request, 'musicform/status.html', context)
#     return render(request, 'musicform/search.html')
#
#
# def about(request):
#     return render(request, 'musicform/about.html')
#
#
# def validate_username(request):
#     username = request.GET.get('username', None)
#     data = {
#         'is_taken': User.objects.filter(username__iexact=username).exists()
#     }
#     return JsonResponse(data)
#
# def user_request(request, band_id):
#     form = RequestForm(request.POST or None)
#     band = get_object_or_404(Band, pk=band_id)
#     if form.is_valid():
#         request = form.save(commit=False)
#         request.user_id_from = request.user
#         request.user_id_to = band.owner
#         request.save()
#         return HttpResponseRedirect('/profile/')
#     context = {
#         'form': form
#     }
#     return render(request, 'musicform/profile/new_request.html', context)
#
#
# def list_bands(request):
#     band_list = Band.objects.all()
#     paginator = Paginator(band_list, 5) #Show 5 objects each page
#
#     page = request.GET.get('page')
#
#     try:
#         bands = paginator.page(page)
#     except PageNotAnInteger:
#         page = paginator.page(1)
#     except EmptyPage:
#         bands = paginator.page(paginator.num_pages)
#
#     return render(request, 'list.html', {'bands': bands})
