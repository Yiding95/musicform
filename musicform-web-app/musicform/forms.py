from django import forms
from .models import UserProfile, Band, BandComments, Song, Instrument, Genre, Requests
from django.contrib.auth.models import User
from django.forms import extras
from django.forms import ModelForm, Textarea
from django.forms.extras.widgets import SelectDateWidget

class BandForm(ModelForm):

    class Meta:
        model = Band
        fields = ['name', 'introduction', 'logo']

        widgets = {
            'introduction': Textarea(attrs={'cols': 70, 'rows': 20}),
        }


class UserForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']


class CommentForm(ModelForm):
    class Meta:
        model = BandComments
        fields = ['text']


class InstrumentForm(ModelForm):
    class Meta:
        model = Instrument
        fields = ['instrument', 'grade']


class SongForm(ModelForm):
    class Meta:
        model = Song
        fields = ['song_name', 'file']


class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = ['genre']


class RequestForm(forms.ModelForm):
    class Meta:
        model = Requests
        fields = ['text']


class UserProfileForm(ModelForm):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    LOCATION_CHOICES = (
        ('Sydney', 'Sydney'),
        ('Canberra', 'Canberra'),
        ('Brisbane', 'Brisbane'),
        ('Melbourne', 'Melbourne'),
        ('Tasmania', 'Tasmania'),
        ('Darwin', 'Darwin'),
        ('Adelaide', 'Adelaide'),
        ('Perth', 'Perth'),
    )
    PROFILE_ICONS = (
        ('bells.png', 'Bell'),
        ('cloud.png', 'Cloud'),
        ('disc.png', 'Disc'),
        ('drum.png', 'Drums'),
        ('eq.png', 'Equaliser'),
        ('guitar.png', 'Guitar'),
        ('mic.png', 'Microphone'),
        ('piano.png', 'Piano'),
        ('sound.png', 'Sound'),
        ('soundboard.png', 'Soundboard'),
        ('tape.png', 'Tape'),
        ('tape2.png', 'Radio'),
        ('violin.png', 'Violin'),
        ('xylo.png', 'Xylophone'),
    )
    birth_date = forms.DateField(widget=extras.SelectDateWidget(years=range(1930,2016)))
    gender = forms.ChoiceField(choices=GENDER_CHOICES, required=True)
    location = forms.ChoiceField(choices=LOCATION_CHOICES, required=True)
    profile_icon = forms.ChoiceField(choices=PROFILE_ICONS, required=True)

    class Meta:
        model = UserProfile
        fields = ['biography', 'birth_date', 'gender', 'location', 'mobile_number', 'profile_icon']



# class LoginForm(forms.ModelForm):
#     class Meta:
#         model = User
#         fields = ('username', 'email')
