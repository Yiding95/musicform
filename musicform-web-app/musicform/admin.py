from django.contrib import admin
from .models import (UserProfile, Requests, Song,
                     Band, BandComments, Genre,
                     Instrument)


admin.site.register(UserProfile)
admin.site.register(Song)
admin.site.register(Instrument)
admin.site.register(Band)
admin.site.register(Genre)
admin.site.register(BandComments)
admin.site.register(Requests)

# Register your models here.
