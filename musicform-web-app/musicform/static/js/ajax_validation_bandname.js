$(function(ready){
    $("#id_bandname").change(function () {
        // console.log( $(this).val() );
        var band_name = $(this).val();

        $.ajax({
            url: '/ajax/validate_bandname/',
            data: {
                'band_name': band_name
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken) {
                    sweetAlert({
                        title: "Oops!",
                        text: "Band name '" + band_name + "' is already taken!",
                        type: "error"
                    });
                    document.getElementById('submit_band').style.visibility = 'hidden';
                } else {
                    sweetAlert({
                        title: "Success!",
                        text: "Band name '" + band_name + "' is available!",
                        type: "success"
                    });
                    document.getElementById('submit_band').style.visibility = 'visible';
                }
            }
        });
    });
});