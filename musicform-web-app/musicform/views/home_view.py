from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from ..models import Band
from django.contrib.auth.models import User


class TemplateView(TemplateView):
    template_name = 'musicform/home.html'


@login_required
def search(request):
    if request.method == "GET":
        if request.GET.get('result'):
            print request.GET.get('filter_by')
            result = Band.objects.filter(name__contains=request.GET['result'])
            if 'Band' == request.GET.get('filter_by'):
                result = Band.objects.filter(name__contains=request.GET['result'])
            elif 'User' == request.GET.get('filter_by'):
                print request.GET['result']
                result = User.objects.filter(username__contains=request.GET['result'])
                print result
                context = {
                    'users': result,
                }
                return render (request, 'musicform/profile/search_user.html', context)
            if 'location' in request.GET:
                search_location = request.GET.get('location')
                result = Band.objects.filter(location__contains=search_location)
            if not result:
                context = {
                    'bands': result,
                }
            else:
                context = {
                    'bands': result,
                    'error_message': 'No results found'
                }
            return render(request, 'musicform/status.html', context)
    return render(request, 'musicform/search.html')

