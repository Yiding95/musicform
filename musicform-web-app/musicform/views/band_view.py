from django.shortcuts import render, get_object_or_404, render_to_response, redirect

from django.http import HttpResponseRedirect, HttpResponseNotFound, Http404, HttpResponse
from django.http import JsonResponse

from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.db import models
from django.db.models import Q
from django.template import RequestContext
from django.utils import timezone

from django.views import generic
from django.views.generic import CreateView, View

from django.urls import reverse

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import IntegrityError

from ..models import Band, BandComments, Song, Instrument, Genre, Requests, UserProfile
from ..forms import UserForm, UserProfileForm, BandForm, CommentForm, SongForm, InstrumentForm, GenreForm, RequestForm

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
SONG_FILE_TYPES = ['mp3', 'wav']


@login_required
def band_new(request):
    """
    The code below will help create a new band,
    firstly checking if the user is logged in first, and
    another check to distinguish whether the user is actually authorised.

    The band will then update the fields which relate to the model of 'Band'
    This includes the file being chosen and the creator which relates to the user
    that requested.

    If the form sent is valid, the information will then send the information to the
    database.
    """
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    else:
        form = BandForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            band = form.save(commit=False)
            band.published_date = timezone.now()
            band.logo = request.FILES['logo']
            band.creator = request.user
            band.location = request.user.userprofile.location
            file_type = band.logo.url.split('.')[-1]
            file_type = file_type.lower()
            if file_type not in IMAGE_FILE_TYPES:
                context = {
                    'band': band,
                    'error_message': 'Invalid Image Type',
                }
                return render(request, 'musicform/band/new_band.html', context)
            band.save()
            return HttpResponseRedirect('/status/')
        context = {
            "form": form
        }
        return render(request, 'musicform/band/new_band.html', context)


@login_required
def band_information(request, band_id):
    """
    This will pull the information from a band with the associated
    primary key.

    Using the method of 'get_object_or_404' will handle if the information
    provided will raise a 404 error. Otherwise it will get information of that
    current band.

    The fields within the context variable will include a user and a band,
    which will allow the template within the render to get the appropriate information
    """
    user = request.user
    band = get_object_or_404(Band, pk=band_id)

    comment_form = CommentForm(request.POST or None)
    if comment_form.is_valid():
        comment = comment_form.save(commit=False)
        comment.author = request.user
        comment.band = band
        comment.save()

    context = {
        'user': user,
        'band': band,
        'comment_form': comment_form,
    }
    return render(request, 'musicform/band/band_information.html', context)


@login_required
def edit_band(request, band_id):
    """
    The edit functions:

    This function is similar to creating a band, but instead using the keyword
    'instance' will get the information from the primary key and pull all the
    appropriate information about the given model.
    """
    band = get_object_or_404(Band, pk=band_id)
    if request.method == "POST":
        form = BandForm(request.POST, instance=band)
        if form.is_valid():
            band = form.save(commit=False)
            band.creator = request.user
            band.published_date = timezone.now()
            band.save()
            return HttpResponseRedirect(reverse('band_information', args=(band.id,)))
    else:
        form = BandForm(instance=band)
    return render(request, 'musicform/band/edit_band.html', {'form': form})


@login_required
def edit_comment(request, pk):
    """
    The edit functions:

    This function is similar to creating a comment, but instead using the keyword
    'instance' will enable to get the information from the primary key and pull all the
    appropriate information about the given model.
    """
    comment = get_object_or_404(BandComments, pk=pk)
    band_pk = comment.band.pk
    if request.method == "POST":
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.updated_date = timezone.now()
            comment.save()
            return HttpResponseRedirect(reverse('band_information', args=(band_pk,)))
    else:
        form = CommentForm(instance=comment)
    return render(request, 'musicform/band/edit_comment.html', {'form': form})




@login_required
def remove_comment(request, pk):
    """
    The remove functions:

    Remove the given comment provided by a primary key
    """
    comment = get_object_or_404(BandComments, pk=pk)
    band_pk = comment.band.pk
    comment.delete()
    return HttpResponseRedirect(reverse('band_information', args=(band_pk,)))


@login_required
def status(request):
    """
    This function retrieves all bands in the given function
    """
    context = {
        'bands': Band.objects.all()
    }
    return render(request, 'musicform/status.html', context)


@login_required
def request(request):
    band_request = Requests.objects.filter(user_id_from=request.user,
                                           hide_function='False')
    context = {
        'requests': band_request
    }
    return render(request, 'musicform/request.html', context)


def user_request(request, band_id):
    form = RequestForm(request.POST or None)
    band = get_object_or_404(Band, pk=band_id)
    if form.is_valid():
        band_request = form.save(commit=False)
        band_request.user_id_from = request.user
        band_request.user_id_to = band.creator
        band_request.band = band
        try:
            band_request.save()
        except IntegrityError:
            context = {
                'form': form,
                'error': 'Sorry, already requested.'
            }
            return render(request, 'musicform/profile/new_request.html', context)
        return HttpResponseRedirect('/request/')
    context = {
        'form': form
    }
    return render(request, 'musicform/profile/new_request.html', context)

@login_required
def manage_request(request):
    context = {
        'requests': Requests.objects.filter(user_id_to=request.user,
                                            hide_function='False')
    }
    return render(request, 'musicform/request.html', context)


def approve_request(request, pk):
    band_request = get_object_or_404(Requests, pk=pk)
    band_request.approve()
    return redirect('band_information', band_id=band_request.band.id)


def decline_request(request, pk):
    band_request = get_object_or_404(Requests, pk=pk)
    band_pk = band_request.band.pk
    band_request.delete()
    return redirect('band_information', band_id=band_pk)


'''
AJAX functions to fulfill the specifications
'''


def get_current_weather(request):
    """
    Provided with a string regarding the location, pull the current weather data
    """
    return 1


def like_band(request):
    """
    Provided with a primary key of a band, allow the user to like a band
    """
    return 1


def favourite_band(request):
    """
    Provided with a primary key of a band, allow the user to favourite a band
    """
    return 1


def validate_bandname(request):
    band_name = request.GET.get('band_name', None)
    if band_name is None:
        return HttpResponse(status=400)
    data = {
        'is_taken': Band.objects.filter(name__iexact=band_name).exists()
    }
    return JsonResponse(data)


def validate_request(request):
    band_requester = request.GET.get('band_requester', None)
    band_owner = request.GET.get('band_owner', None)
    if band_requester is None:
        return HttpResponse(status=400)
    data = {
        'already_member': Requests.objects.filter(user_id_to=band_requester).filter(user_id_from=band_owner).exists()
    }
    return JsonResponse(data)