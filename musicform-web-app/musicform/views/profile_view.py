from django.shortcuts import render, get_object_or_404, render_to_response, redirect

from django.http import HttpResponseRedirect, HttpResponseNotFound, Http404
from django.http import JsonResponse

from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.db import models
from django.db import IntegrityError
from django.template import RequestContext

from django.utils import timezone
from django.utils.decorators import method_decorator

from django.views import generic
from django.views.generic import CreateView, View

from django.urls import reverse

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.views.generic import TemplateView

from ..models import Band, Song, Instrument, Genre, Requests, UserProfile
from ..forms import UserForm, UserProfileForm, SongForm, InstrumentForm, GenreForm

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
SONG_FILE_TYPES = ['mp3', 'wav']


# Administration Links
def register(request):

    context = RequestContext(request)

    registered = False

    user_form = UserForm(request.POST or None)
    profile_form = UserProfileForm(request.POST or None)

    if request.method == "POST":
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registered = True
        else:
            value = {
                'user_form': user_form,
                'profile_form': profile_form,
                'user_error': user_form.errors,
                'profile_error': profile_form.errors,
                'registered': registered,
            }
            return render(request, 'musicform/register.html', value, context)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    value = {
        'user_form': user_form,
        'profile_form': profile_form,
        'registered': registered,
    }
    return render(request, 'musicform/register.html', value, context)


# Login URLs
def login_form(request):
    context = RequestContext(request)

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                # bands = Bands.objects.filter(user=request.user)
                return HttpResponseRedirect('/profile/')
            else:
                return HttpResponseRedirect("Your account has been disabled")
        else:
            context = {
                'error_message': 'Invalid Password/Username',
            }
            return render(request, 'musicform/login.html', context)
    else:
        return render(request, 'musicform/login.html', {}, context)


def edit_profile(request):
    context = RequestContext(request)

    user_prof = request.user.userprofile

    if request.method == "POST":
        profile_form = UserProfileForm(request.POST, instance=user_prof)
        user_form = UserForm(request.POST, instance=user_prof)

        if profile_form.is_valid() and user_form.is_valid():
            user = user_form.save(commit=False)
            user.save()
            profile = profile_form.save(commit=False)
            profile.save()
        else:
            return render(request, 'musicform/profile/edit_profile.html', context)
    else:
        profile_form = UserProfileForm(instance=user_prof)
        user_form = UserForm(instance=request.user)

    value = {
        'user_form': user_form,
        'profile_form': profile_form
    }
    return render(request, 'musicform/profile/edit_profile.html', value, context)


@login_required
def send_request(request):
    users = User.objects.all()
    return render(request, 'musicform/band/send_request.html', {'users': users})


@method_decorator(login_required, name='dispatch')
class LogoutDetail(TemplateView):
    template_name = 'musicform/logout.html'

    def get(self, request, *args, **kwargs):
        auth.logout(request)
        return super(LogoutDetail, self).get(request, *args, **kwargs)


# Band URLs
def request(request):
    return render(request, 'musicform/band/request.html')


@login_required
def view_request(request):
    return render(request, 'musicform/band/view_requests.html')


def view_other_prof(request, prof_id):
    user = get_object_or_404(User, pk=prof_id)
    context = {
        'user': user,
        'user_prof': get_object_or_404(UserProfile, user=user),
        'bands': Band.objects.filter(creator=prof_id),
        'songs': Song.objects.filter(uploader=prof_id),
        'instruments': Instrument.objects.filter(owner=prof_id),
        'genres': Genre.objects.filter(owner=prof_id)
    }

    return render(request, 'musicform/profile/view_profile.html', context)


class ProfileDetail(TemplateView):
    template_name = 'musicform/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileDetail, self).get_context_data(**kwargs)
        context['bands'] = Band.objects.filter(creator=self.request.user)
        context['songs'] = Song.objects.filter(uploader=self.request.user)
        context['instruments'] = Instrument.objects.filter(owner=self.request.user)
        context['genres'] = Genre.objects.filter(owner=self.request.user)
        context['requests'] = Requests.objects.filter(user_id_from=self.request.user)

        return context


class AllSongsDetail(TemplateView):
    template_name = 'musicform/profile/all_songs.html'

    def get_context_data(self, **kwargs):
        context = super(AllSongsDetail, self).get_context_data(**kwargs)
        context['songs'] = Song.objects.filter(uploader=self.request.user)

        return context


@login_required
def user_genre(request):
    form = GenreForm(request.POST or None)
    if form.is_valid():
        genre = form.save(commit=False)
        genre.owner = request.user
        try:
            genre.save()
        except IntegrityError:
            context = {
                'genre_form': form,
                'error': 'Sorry, already requested.'
            }
            return render(request, 'musicform/profile/new_genre.html', context)
        return HttpResponseRedirect('/profile/')
    context = {
        'genre_form': form
    }
    return render(request, 'musicform/profile/new_genre.html', context)


@login_required
def user_song(request):
    form = SongForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        song = form.save(commit=False)
        song.uploader = request.user
        file_type = song.file.url.split('.')[-1]
        file_type = file_type.lower()
        if file_type not in SONG_FILE_TYPES:
            context = {
                'song': song,
                'error_message': 'Invalid File Type',
            }
            return render(request, 'musicform/profile/song.html', context)
        song.save()
        return HttpResponseRedirect('/profile/')
    context = {
        'form': form
    }
    return render(request, 'musicform/profile/new_song.html', context)


@login_required
def user_instruments(request):
    form = InstrumentForm(request.POST or None)
    if form.is_valid():
        instrument = form.save(commit=False)
        instrument.owner = request.user
        try:
            instrument.save()
        except IntegrityError:
            context = {
                'form': form,
                'error': 'Sorry, already requested.'
            }
            return render(request, 'musicform/profile/new_instrument.html', context)
        return HttpResponseRedirect('/profile/')
    context = {
        'form': form
    }
    return render(request, 'musicform/profile/new_instrument.html', context)


@login_required
def song_information(request, song_id):
    user = request.user
    song = get_object_or_404(Song, pk=song_id)
    context = {
        'user': user,
        'song': song
    }
    return render(request, 'musicform/profile/song.html', context)


def validate_username(request):
    username = request.GET.get('username', None)
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)
